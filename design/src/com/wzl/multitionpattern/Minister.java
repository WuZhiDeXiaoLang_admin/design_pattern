package com.wzl.multitionpattern;

/**
 * @author wuzhilang
 * @Title: Minister
 * @ProjectName design_pattern
 * @Description: TODO
 * @date 2019/2/149:24
 */
public class Minister {
	public static void main(String[] args) {
		int ministerNum = 10;
		for (int i=0;i<ministerNum;i++){
			Emperor emperor = Emperor.getInstance();
			System.out.println("第"+(i+1)+"大臣参拜的是:"+"");
			emperor.emperorInfo();
		}

	}
}
