package com.wzl.multitionpattern;

import java.util.ArrayList;
import java.util.Random;

/**
 * @author wuzhilang
 * @Title: Emperor
 * @ProjectName design_pattern
 * @Description: 多例模式
 * @date 2019/2/1110:42
 */
public class Emperor {
	private static  int maxNumOfEmperor= 2; //最多只能有两个皇帝
	private static ArrayList emperorInfoList = new ArrayList(maxNumOfEmperor);
	private  static  ArrayList emperorList = new ArrayList(maxNumOfEmperor);
	private static  int countNumOfemperor = 0;
	static {
		for (int i = 0;i<maxNumOfEmperor;i++){
			emperorList.add(new Emperor("皇"+(i+1)+"帝"));
		}
	}
	private Emperor(){

	}
	private Emperor(String info){
		emperorInfoList.add(info);
	}
	public  static Emperor  getInstance(){
		Random random = new Random();
		countNumOfemperor = random.nextInt(maxNumOfEmperor);
		return  (Emperor)emperorList.get(countNumOfemperor);
	}
	public   void  emperorInfo(){
		System.out.println(emperorInfoList.get(countNumOfemperor));
	}
}
