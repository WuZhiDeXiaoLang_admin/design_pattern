package com.wzl.abstractfactorypattern;

/**
 * @author wuzhilang
 * @Title: AbstractCat
 * @ProjectName design_pattern
 * @Description: TODO
 * @date 2019/3/818:01
 */
public abstract class AbstractCat {
	public void eat(){
		System.out.println("猫吃");
	}
	public void play(){
		System.out.println("猫玩");
	}
}
