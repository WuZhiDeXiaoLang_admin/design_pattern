package com.wzl.abstractfactorypattern;

import java.util.Arrays;
import java.util.List;
import java.util.jar.JarFile;

/**
 * @author wuzhilang
 * @Title: WhilteCat
 * @ProjectName design_pattern
 * @Description: TODO
 * @date 2019/3/818:34
 */
public class WhilteCat extends AbstractCat{
	public static void main(String[] args) {
		Integer a = 500;
		Integer b = 500;
		int c = 500;
		System.out.println(a == b);
		System.out.println(a == c);

		Integer e = 55;
		Integer f = 55;
		int g = 55;
		System.out.println(e == f);
		System.out.println(e == g);

	}
}
