package com.wzl.abstractfactorypattern;

/**
 * @author wuzhilang
 * @Title: Animal
 * @ProjectName design_pattern
 * @Description: 动物类
 * @date 2019/3/811:40
 */
public interface Animal {
	void eat();
	void play();
}
