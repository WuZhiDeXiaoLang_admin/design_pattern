package com.wzl.abstractfactorypattern;

/**
 * @author wuzhilang
 * @Title: AbstractDog
 * @ProjectName design_pattern
 * @Description: TODO
 * @date 2019/3/811:55
 */
public abstract class AbstractDog implements Animal{
	public  void eat(){
		System.out.println("狗吃");
	}
	public void play(){
		System.out.println("狗玩");
	}
}
