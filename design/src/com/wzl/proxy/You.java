package com.wzl.proxy;

/**
 * @author wuzhilang
 * @Title: You
 * @ProjectName design_pattern
 * @Description: 代理模式的核心
 * @date 2019/1/3115:13
 */
public class You implements Action {
	private Action kindWomen;
	public You(){
		this.kindWomen = new Me();
	}
	public You(Action kindWomen){
		this.kindWomen=kindWomen;
	}
	@Override
	public void eat() {
		this.kindWomen.eat();
	}

	@Override
	public void swim() {
		this.kindWomen.swim();

	}
}
