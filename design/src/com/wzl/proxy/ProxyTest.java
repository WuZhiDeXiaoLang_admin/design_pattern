package com.wzl.proxy;

/**
 * @author wuzhilang
 * @Title: ProxyTest
 * @ProjectName design_pattern
 * @Description: TODO
 * @date 2019/1/3117:14
 */
public class ProxyTest
{
	public static void main(String[] args) {
		You you = new You();
		you.eat();
		you.swim();
	}
}
