package com.wzl.proxy;

/**
 * @author wuzhilang
 * @Title: Action
 * @ProjectName design_pattern
 * @Description: 同一类型的人
 * @date 2019/1/3115:11
 */
public interface Action {
	 void eat();
	 void swim();
}
