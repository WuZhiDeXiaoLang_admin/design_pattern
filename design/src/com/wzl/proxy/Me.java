package com.wzl.proxy;

/**
 * @author wuzhilang
 * @Title: Me
 * @ProjectName design_pattern
 * @Description: TODO
 * @date 2019/1/3115:23
 */
public class Me implements Action {
	@Override
	public void eat() {
		System.out.println("我吃肉");
	}

	@Override
	public void swim() {
		System.out.println("我游泳");
	}
}
