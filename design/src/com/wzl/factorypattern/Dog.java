package com.wzl.factorypattern;

/**
 * @author wuzhilang
 * @Title: Dog
 * @ProjectName design_pattern
 * @Description: 狗
 * @date 2019/2/2018:04
 */
public class Dog implements Animal {
	@Override
	public void play() {
		System.out.println("狗会玩！");
	}

	@Override
	public void eat() {
		System.out.println("狗会吃猫肉！");
	}
}
