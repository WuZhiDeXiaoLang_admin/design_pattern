package com.wzl.factorypattern;

/**
 * @author wuzhilang
 * @Title: Pig
 * @ProjectName design_pattern
 * @Description: 猪
 * @date 2019/2/2018:05
 */
public class Pig implements Animal {
	@Override
	public void play() {
		System.out.println("猪会玩！");
	}

	@Override
	public void eat() {
		System.out.println("猪会吃！");
	}
}
