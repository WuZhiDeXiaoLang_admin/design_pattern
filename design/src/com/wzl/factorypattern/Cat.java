package com.wzl.factorypattern;

/**
 * @author wuzhilang
 * @Title: Cat
 * @ProjectName design_pattern
 * @Description: 猫
 * @date 2019/2/2018:01
 */
public class Cat implements Animal{
	@Override
	public void play() {
		System.out.println("猫会玩毛线！");
	}

	@Override
	public void eat() {
		System.out.println("猫会吃狗肉！");
	}
}
