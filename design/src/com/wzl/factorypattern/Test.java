package com.wzl.factorypattern;

/**
 * @author wuzhilang
 * @Title: Test
 * @ProjectName design_pattern
 * @Description: 测试类
 * @date 2019/2/2111:16
 */
public class Test {
	public static void main(String[] args) {
		Animal cat = AnimalFactory.creatAnimal(Cat.class);
		cat.play();
		cat.eat();
		Animal dog = AnimalFactory.creatAnimal(Dog.class);
		dog.play();
		dog.eat();
		Animal pig = AnimalFactory.creatAnimal(Pig.class);
		pig.play();
		pig.eat();
	}
}
