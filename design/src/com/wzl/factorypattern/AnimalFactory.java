package com.wzl.factorypattern;

/**
 * @author wuzhilang
 * @Title: AnimalFactory
 * @ProjectName design_pattern
 * @Description: TODO
 * @date 2019/2/2110:19
 */
public class AnimalFactory {
	public static Animal creatAnimal(Class c){
			Animal a = null;
		try {
			 a = (Animal) Class.forName(c.getName()).newInstance();
		} catch (InstantiationException e) {
			System.out.println("必须指定动物属性！");
		} catch (IllegalAccessException e) {
			System.out.println("定义错误");
		} catch (ClassNotFoundException e) {
			System.out.println("没有找到动物！");
		}
		return  a;
	}
}
