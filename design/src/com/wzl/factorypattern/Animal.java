package com.wzl.factorypattern;

/**
 * @author wuzhilang
 * @Title: Animal
 * @ProjectName design_pattern
 * @Description: 动物行为接口
 * @date 2019/2/2016:05
 */
public  interface Animal {
	 void play();
	 void eat();
}
