package com.wzl.decorated;

/**
 * @author wuzhilang
 * @Title: FiredEgg
 * @ProjectName design_pattern
 * @Description: TODO
 * @date 2018/9/2519:33
 */
public class FiredEgg extends Condiment {
	private Pancake pancake;
	public  FiredEgg(Pancake pancake){
		this.pancake = pancake;
	}
	@Override
	public String getDesc() {
		return pancake.getDesc()+",煎蛋";
	}

	@Override
	public double price() {
		return pancake.price()+2;
	}
}
