package com.wzl.decorated;

/**
 * @author wuzhilang
 * @Title: Ham
 * @ProjectName design_pattern
 * @Description: TODO
 * @date 2018/9/2519:37
 */
public class Ham extends  Condiment {
	private Pancake pancake;
	public Ham(Pancake pancake){
		this.pancake = pancake;
	}
	@Override
	public String getDesc() {
		return pancake.getDesc()+",火腿片";
	}

	@Override
	public double price() {
		return pancake.price()+1.5;
	}
}
