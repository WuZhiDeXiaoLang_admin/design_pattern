package com.wzl.decorated;

/**
 * @author wuzhilang
 * @Title: Roujiamo
 * @ProjectName design_pattern
 * @Description: TODO
 * @date 2018/9/2519:32
 */
public class Roujiamo extends Pancake {
	public Roujiamo(){
		desc = "肉夹馍";
	}
	@Override
	public double price() {
		return 6;
	}
}
