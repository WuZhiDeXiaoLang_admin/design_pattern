package com.wzl.decorated;

/**
 * @author wuzhilang
 * @Title: Condiment
 * @ProjectName design_pattern
 * @Description: TODO
 * @date 2018/9/2519:34
 */
public abstract class Condiment extends Pancake {
	public abstract String getDesc();
}
