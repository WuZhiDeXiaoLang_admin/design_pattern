package com.wzl.decorated;

import org.junit.jupiter.api.Test;

/**
 * @author wuzhilang
 * @Title: MyTest
 * @ProjectName design_pattern
 * @Description: TODO
 * @date 2018/9/2519:40
 */
public class MyTest {
	@Test
	public void test() {
		Pancake tornCake = new TornCake();
		//手抓饼基础价
		System.out.println(String.format("%s ￥%s", tornCake.getDesc(), tornCake.price()));

		Pancake roujiamo = new Roujiamo();
		roujiamo = new FiredEgg(roujiamo);
		roujiamo = new FiredEgg(roujiamo);
		roujiamo = new Ham(roujiamo);
		//我好饿
		System.out.println(String.format("%s ￥%s", roujiamo.getDesc(), roujiamo.price()));
	}
}
