package com.wzl.decorated;

/**
 * @author wuzhilang
 * @Title: Pancake
 * @ProjectName design_pattern
 * @Description: TODO
 * @date 2018/9/2519:29
 */
public abstract class Pancake {

	public String desc = "我不是一个具体的煎饼";
	public  String getDesc(){
		return  desc;
	}
	public  abstract  double price();
}
