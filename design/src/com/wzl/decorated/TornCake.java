package com.wzl.decorated;

/**
 * @author wuzhilang
 * @Title: TornCake
 * @ProjectName design_pattern
 * @Description: TODO
 * @date 2018/9/2519:31
 */
public class TornCake  extends  Pancake{
	public  TornCake(){
		desc = "手抓饼";
	}
	@Override
	public double price() {
		return 4;
	}
}
