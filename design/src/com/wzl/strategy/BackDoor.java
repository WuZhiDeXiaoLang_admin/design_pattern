package com.wzl.strategy;

/**
 * @author wuzhilang
 * @Title: BackDoor
 * @ProjectName design_pattern
 * @Description: TODO
 * @date 2019/1/3114:37
 */
public class BackDoor implements  IStrategy{
	@Override
	public void operate() {
		System.out.println("你可以吃肉");
	}
}
