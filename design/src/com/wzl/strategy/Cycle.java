package com.wzl.strategy;

/**
 * @author wuzhilang
 * @Title: Cycle
 * @ProjectName design_pattern
 * @Description: TODO
 * @date 2019/1/3114:38
 */
public class Cycle implements  IStrategy {
	@Override
	public void operate() {
		System.out.println("你可以骑车");
	}
}
