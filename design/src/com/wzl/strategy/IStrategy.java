package com.wzl.strategy;

/**
 * @author wzl
 * @Title: IStrategy
 * @ProjectName design_pattern
 * @Description: 策略的接口
 * @date 2019/1/3114:29
 */
public interface IStrategy {
//	/每个锦囊妙计都是一个可执行的算法
	 void operate();
}
