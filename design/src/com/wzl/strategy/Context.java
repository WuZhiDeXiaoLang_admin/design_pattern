package com.wzl.strategy;

/**
 * @author wuzhilang
 * @Title: Context
 * @ProjectName design_pattern
 * @Description: 策略的存放处
 * @date 2019/1/3114:44
 */
public class Context {
	private IStrategy strategy;
	public Context(IStrategy strategy){
		this.strategy = strategy;
	}
	//使用计谋了，看我出招了
	public void operate(){
		this.strategy.operate();
	}
}
