package com.wzl.strategy;

/**
 * @author wuzhilang
 * @Title: Ming
 * @ProjectName design_pattern
 * @Description: 策略的执行者
 * @date 2019/1/3114:56
 */
public class Ming {

	public static void main(String[] args) {
		Context context;
		//通过传入参数的区别应用不同的策略
		context = new Context(new BackDoor());
		context.operate();
		context = new Context(new Cycle());
		context.operate();
		context = new Context(new Car());
		context.operate();

	}
}
