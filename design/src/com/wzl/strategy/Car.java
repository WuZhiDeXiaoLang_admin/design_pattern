package com.wzl.strategy;

/**
 * @author wuzhilang
 * @Title: Car
 * @ProjectName design_pattern
 * @Description: TODO
 * @date 2019/1/3114:39
 */
public class Car implements IStrategy {

	@Override
	public void operate() {
		System.out.println("你可以开车");
	}
}
