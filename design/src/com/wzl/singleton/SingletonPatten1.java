package com.wzl.singleton;

/**
 * @author wuzhilang
 * @Title: SingletonPatten1
 * @ProjectName design_pattern
 * @Description: 多线程状态下如何确保单例的实现。
 * @date 2019/2/119:43
 */
public class SingletonPatten1 {
	private  static  final SingletonPatten1 singletonPatten1  = new SingletonPatten1();
	private SingletonPatten1(){

	}
	public synchronized static SingletonPatten1 getInstance(){
		return singletonPatten1;
	}
}
