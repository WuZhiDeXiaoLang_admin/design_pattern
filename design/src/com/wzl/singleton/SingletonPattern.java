package com.wzl.singleton;

/**
 * @author wuzhilang
 * @Title: SingletonPattern
 * @ProjectName design_pattern
 * @Description: TODO
 * @date 2019/2/119:39
 */
public class SingletonPattern {
	private  static  SingletonPattern singletonPattern = null;
	private  SingletonPattern(){

	}
	private SingletonPattern getInstance(){
		if(this.singletonPattern == null){
			this.singletonPattern = new SingletonPattern();
		}
		return  this.singletonPattern;
	}

}
