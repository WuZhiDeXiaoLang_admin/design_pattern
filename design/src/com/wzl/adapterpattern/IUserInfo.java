package com.wzl.adapterpattern;

/**
 * @author wuzhilang
 * @Title: IUserInfo
 * @ProjectName design_pattern
 * @Description: 员工信息接口
 * @date 2019/6/1215:15
 */
public interface IUserInfo {
//	获取用户姓名
	String getUserName();
//	获取家庭地址
	String getHomeAddress();
//	手机号码
	String getMpbileNumber();
//	办公电话
	String getOfficeTelNumber();
//	这个人的职位是什么
	String getJobPosition();
//	获得家庭电话
	String getHomeTelNumber();

}
