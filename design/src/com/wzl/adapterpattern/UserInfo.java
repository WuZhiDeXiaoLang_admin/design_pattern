package com.wzl.adapterpattern;

/**
 * @author wuzhilang
 * @Title: UserInfo
 * @ProjectName design_pattern
 * @Description: 员工信息
 * @date 2019/6/1219:46
 */
public class UserInfo implements IUserInfo{
	@Override
	public String getUserName() {
		System.out.println("姓名为王");
		return null;
	}

	@Override
	public String getHomeAddress() {
		System.out.println("这里是员工的家");
		return null;
	}

	@Override
	public String getMpbileNumber() {
		System.out.println("这里是员工的电话号码");
		return null;
	}

	@Override
	public String getOfficeTelNumber() {
		System.out.println("办公室的办公电话");
		return null;
	}

	@Override
	public String getJobPosition() {
		System.out.println("员工的职位是最低级");
		return null;
	}

	@Override
	public String getHomeTelNumber() {
		return null;
	}
}
