package com.wzl.proxymanagerpattern;



/**
 * @author wuzhilang
 * @Title: Client
 * @ProjectName design_pattern
 * @Description: 场景类
 * @date 2019/6/2720:17
 */
public class Client {
	public static void main(String[] args) {
//然后再定义一个代练者
		ImGamePlayer proxy = new GamePlayerProxy("张三");
//开始打游戏，记下时间戳
		System.out.println("开始时间是：2009-8-25 10:45");
		proxy.logIn("zhangSan", "password");
//开始杀怪
		proxy.killBoos();
//升级
		proxy.upGrade();
//记录结束游戏时间
		System.out.println("结束时间是：2009-8-26 03:40");
	}


}
