package com.wzl.proxymanagerpattern;


/**
 * @author wuzhilang
 * @Title: GamePlayer
 * @ProjectName design_pattern
 * @Description: 游戏者
 * @date 2019/6/2720:11
 */
public class GamePlayer  implements  ImGamePlayer{
	private String name = "";
	public GamePlayer (ImGamePlayer _gamePlayer,String _name) throws Exception{
		if(_gamePlayer == null){
			throw new Exception("不能创建真实角色!");
		}else{
			this.name = _name;
		}
	}
	@Override
	public void logIn(String userName, String password) {
		System.out.println("登录名为"+ name +"用户"+this.name +"登录成功!" );
	}

	@Override
	public void killBoos() {
		System.out.println(this.name +"在打怪");

	}

	@Override
	public void upGrade() {
		System.out.println(this.name+"又升了一级");

	}
}
