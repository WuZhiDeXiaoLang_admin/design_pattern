package com.wzl.proxymanagerpattern;

/**
 * @author wuzhilang
 * @Title: RealSubject
 * @ProjectName design_pattern
 * @Description: TODO
 * @date 2019/7/217:36
 */
public class RealSubject implements Subject{
	@Override
	public void request() {
		System.out.println();
	}
}
