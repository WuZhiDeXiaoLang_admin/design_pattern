package com.wzl.proxymanagerpattern;

/**
 * @author wuzhilang
 * @Title: GamePlayerProxy
 * @ProjectName design_pattern
 * @Description: 普通代理的代理者
 * @date 2019/7/314:03
 */
public class GamePlayerProxy implements ImGamePlayer{
	private  ImGamePlayer gamePlayer = null;
	public  GamePlayerProxy(String name){
		try {
			gamePlayer  = new GamePlayer(this,name);
		} catch (Exception e) {
//			异常处理
			e.printStackTrace();
		}
	}
//	代理登录
	@Override
	public void logIn(String userName, String password) {
		this.gamePlayer.logIn(userName,password);

	}
// 代理杀怪
	@Override
	public void killBoos() {
		this.gamePlayer.killBoos();
	}
//代理升级
	@Override
	public void upGrade() {
		this.gamePlayer.upGrade();

	}
}
