package com.wzl.proxymanagerpattern;

/**
 * @author wuzhilang
 * @Title: ImGamePlayer
 * @ProjectName design_pattern
 * @Description: 游戏者接口
 * @date 2019/6/2720:08
 */
public interface ImGamePlayer {
	 void logIn( String userName,String password);
	 void killBoos();
	 void upGrade();
}
