package com.wzl.proxymanagerpattern;

/**
 * @author wuzhilang
 * @Title: Subject
 * @ProjectName design_pattern
 * @Description:  抽象主题类
 * @date 2019/7/215:59
 */
public interface Subject {
	void  request();
}
