# 设计模式

#### Description
诸葛小狼的设计模式小demo

#### Software Architecture
Software architecture description

Java语言描述的设计模式
#### Instructions

1. decorated 装饰者模式
2. multition 多例模式
3. proxy 代理模式  
代理模式：Subject 抽象主题角色 抽象主题可以是一个抽象类或者一个接口  
          RealSubject  具体主题角色 也叫作被委托角色，被代理角色。  
          ProxySubject 代理主题角色 也叫作委托类，代理类
          
          
4. singleton 单例模式
5. strategy 策略者模式
6. factorypattern 工厂方法模式
7. 抽象工厂方法模式

#### Contribution

1. Fork the project
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)